var gulp          = require('gulp'),
    changed       = require('gulp-changed'),
    compass       = require('gulp-compass'),
    autoprefixer  = require('gulp-autoprefixer'),
    minifycss     = require('gulp-minify-css'),
    imagemin      = require('gulp-imagemin'),
    jshint        = require('gulp-jshint'),
    notify        = require('gulp-notify'),
    plumber       = require('gulp-plumber'),
    watch         = require('gulp-watch'),
    connect       = require('gulp-connect');


// Gulp plumber error handler
var onError = function(err) {
  console.log(err);
}

gulp.task('connectDev', function () {
   connect.server({
//     port: 8888,
//     host: 'localhost',
    livereload: true
  });
});

gulp.task('html', function () {
  gulp.src('*.html')
    .pipe(connect.reload());
});

gulp.task('style', function() {
  gulp.src('sass/style.sass')
  .pipe(compass({
    css: 'css',
    sass: 'sass',
    style: 'expanded',
    sourcemap: true,
  }))
  .pipe(autoprefixer({ browsers: ['last 2 version', 'safari 5', 'ie 9', 'opera 12.1']}))
  .pipe(gulp.dest('css'))
  .pipe(connect.reload())
  .pipe(notify({ message: 'SASS : - On a chromé les jantes' }));
});

gulp.task('styleProd', function() {
  gulp.src('sass/**/*.sass')
  .pipe(compass({
    css: 'css',
    sass: 'sass',
    style: 'compressed',
    sourcemap: false,
  }))
  .pipe(autoprefixer({ browsers: ['last 2 version', 'safari 5', 'ie 9', 'opera 12.1']}))
  .pipe(gulp.dest('css'))
  .pipe(notify({ message: 'SASS : - On a chromé les jantes' }));
});

// gulp.task('images', function() {
//   var imgSrc = 'sites/all/themes/pilot/images/**/*',
//       imgDst = 'sites/all/themes/pilot/images/**/*';

//   return gulp.src(imgSrc)
//       .pipe(plumber({
//           errorHandler: onError
//       }))
//       .pipe(changed(imgDst))
//       .pipe(imagemin())
//       .pipe(gulp.dest(imgDst))
//       .pipe(notify({ message: 'Images task complete' }));
// });

// // Hint all of our custom developed Javascript to make sure things are clean
// gulp.task('jshint', function() {
//   return gulp.src(['sites/all/modules/custom/**/*.js','sites/all/themes/pilot/**/*.js'])
//   .pipe(plumber({
//     errorHandler: onError
//   }))
//   .pipe(jshint())
//   .pipe(jshint.reporter('default'))
//   .pipe(notify({ message: 'JS Hinting task complete' }));
// });

gulp.task('watch', function() {

  gulp.watch(['*.html'], ['html']);
  gulp.watch('sass/**/*.sass', ['style']);
  
  //gulp.watch('sites/all/**/*.js', ['jshint']);

  //gulp.watch('sites/all/**/*.png', ['images']);

});


gulp.task('build', function () { console.log('Working!'); });


gulp.task('default', ['style', 'connectDev', 'watch']);
gulp.task('prod', ['styleProd', 'jshint']);