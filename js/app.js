define([
  'jquery',
  'underscore',
  'marionette',
  'models/user',
  'models/organisation',
], function($, _, Marionette, User, Organisation){
  
  var app = new Marionette.Application();

  app.addInitializer(function() {

    this.addRegions({
      main: {
        selector: '#page',
      },
    });

    this.user = new User();
    this.organisation = new Organisation();

  });

  app.on("initialize:after", function(){
    
    Backbone.history.start({ pushState: true });
  
  });

  return app;

});