define([
  'config',
  'jquery',
  'backbone',
  'views/home',
  'views/main',
  'views/signUpProfile',
  'collections/contacts'
], function(config, $, Backbone, HomeView, MainView, SignUpProfileView, Contacts) {

  var app;

  function ApiManager(_app) {
    app = _app;
    this.loadGapi();
  }

  _.extend(ApiManager.prototype, Backbone.Events);

  ApiManager.prototype.init = function() {
    var self = this;

    gapi.client.load('tasks', 'v1', function() { self.checkAuth(); });

    function handleClientLoad() {
      gapi.client.setApiKey(config.apiKey);
      window.setTimeout(checkAuth, 100);
    }

    function checkAuth() {
      gapi.auth.authorize({ client_id: config.clientId, scope: config.scopes, immediate: true }, handleAuthResult);
    }

    function handleAuthResult(authResult) {
      var authTimeout;

      if (authResult && !authResult.error) {
        // Schedule a check when the authentication token expires
        if (authResult.expires_in) {
          authTimeout = (authResult.expires_in - 5 * 60) * 1000;
          setTimeout(checkAuth, authTimeout);
        }

        //app.views.auth.$el.hide();
        //$('#signed-in-container').show();

        //console.log(authResult);

        //app.main.show(new MainView());

        getInfos();

      } else {
        
        if (authResult && authResult.error) {
          // TODO: Show error
          console.error('Unable to sign in:', authResult.error);
        }

        app.main.show(new HomeView());
      }
    }

    function getInfos(){
      // Charger les bibliothèques OAuth2 pour activer les méthodes userinfo.
      gapi.client.load('oauth2', 'v2', function() {
        var request = gapi.client.oauth2.userinfo.get();
        request.execute(getInfosCallback);
      });
    }

    function getInfosCallback(infos){

      var user_gplus_id = infos.id;
      var organisation = infos.hd;
      console.log(infos);

      $.ajax({
        url: "http://localhost:3000/users/gplus/"+user_gplus_id,
      }).done(function(data){

        if(!data.error){
          
          var me = data;

          console.log(data);

          app.user.set(data);

          console.log(app.user);

          var contacts = [{
            name: 'Michel Test',
            firstname: 'Michel',
            lastname: 'Test',
            email: 'michel.test@dolmen-tech.com',
            photo: 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBwgHBgkIBwgKCgkLDRYPDQwMDRsUFRAWIB0iIiAdHx8kKDQsJCYxJx8fLT0tMTU3Ojo6Iys/RD84QzQ5OjcBCgoKDQwNGg8PGjclHyU3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3N//AABEIAFoAWgMBIgACEQEDEQH/xAAcAAEAAwEBAAMAAAAAAAAAAAAABQYHBAMBAgj/xAA0EAABAwMCAwUGBQUAAAAAAAABAAIDBAUREiEGMVETIkFhgQcUIzKRsUJxc6HBFSRSguH/xAAUAQEAAAAAAAAAAAAAAAAAAAAA/8QAFBEBAAAAAAAAAAAAAAAAAAAAAP/aAAwDAQACEQMRAD8A3FERAREQEREBERAREQEREBFx3O50VppX1VxqGwQMaXOe7OwAyeXkFWLlxhYbxY6s2y8ljmxPIe2JzcENPPUBjHntsgstfebXbcC4XGkpiTgCaZrST5Alcj+KLQ1ups8sjf8AKGllkH1a0rHrb7Pr42nju1pqKWvFXEHCoedD3NdvnTzzjq4nyXlbOCb7R1Us9z0xBzQA5/aHHPJyG4QbC7i21huoe+FvU0Urfu0KId7U+E2SujfV1Qc0kH+xmO/o1Z7S0tqt75I7he6N8zjqwGujPrkKj3PtHXqWpigMsUcmIiHAtcAefy4+uEH6NtfGtjuzC+3y1kzQcEtt9RgH89Cl2XGncNzKz9SF7PuAsQskzamlc+ulp6J5wGBrCSR1JYDlSlRTVs1tMNsukerHzRulifn1YMemEGwQ1VPOS2CeKQjmGPBI+i9lg1RU8R0Vvc+qrqrs6cl5kqvjFrQDvh4cD4fKWnzCvfBHF9th4aglvvENH22XAiaYBzcPIGcucfDbJz132QX5Fx2+62+5s12+shqG9Y3grsQec8LZmaXD9sqJu1NcvdpDRGOUhpxFJyd+Wd2nockeSml8HdBXuE7jDcbUZqaLs4WzyMYzGnADj4eCl5H4GcH0UTZaSG0UUkEDJHfGkc5uc97O+OWBt+/NeklU90j/AIgwzPdYDoB74OXfi3b8o3aefNBmvtQq428acLSv7RscXvAe98TwBkDGNt/RfatudG6LaaIn9Jw/hcvtce+pu1nfENUNM97pTqHdB0tG2c7k+CrtRM0s8EGq8MVLP6LTkuGktOnSx3UrtqauPHJ5/wBSPuoLhibTY6JpcB8MDJ8MrorKjuDS9rXbZDm52zv+338UFb49q3usdTHTMeJXlrGHUAQS4AdVPcD8MVFFw1Q0bLZbNcbXCSqkha973FxJ7xyQQTjGkjbmqbxRdfdhSPMJmk95izTsJy52r5Qcc8jp0W3WKidbrNRUbzmSGFrHu6ux3j9coIqy8JUluuYush7SuEbo2lo0MYHY1YaNsnSN8DlyCsaIgIiIIG/CSgkNewB1M4BtUwjOOQ1/ljY+nmoG7XyNkGuLVkDcHct5YA8BtnzGVengFpDgCMb5WUXy211X7xPw1bnz0Xa6WNjkB0g8yASNjscAnAxnGcAMz43vktRXwvAa6RhIDiMlm7TlvQ5bzHPKjBcpHjv/ADP3GTu7P3XdxPar9Sz4rLLcGNa4ZJp3luAc/MNj9VW+2YBpLiHDTqY5uCTqJ3Hj66uaDVbBfIoqSNgaA5oBJacHOP8AoP8AK6qy8sk1Bmt4YdTQDgOwOZAPQFZ9QuuUsTfdKCrew4AIgdp35DOMfZXbgzhetu11bS3z3q30Ze7Q0t700jMEta4HuOAO+xyNQ5ty0JT2e8PScScQw3yvBkt9tk1QFwOJZQBpLT4taDnkCHNA6rZl4UNHTW+kipKKCOCnibpjjjGA0L3QEREBERB9J4mTwvhlGWPaWuGcZBXxTwRU0LIYGBkbBhrR4L0RAREQMKKuFkjq66Csinkp5I5WSP0AYk08s55HGW5H4XEdMSqICIiAiIgIiICIiAiIgIiICIiAiIg//9k=',
            plusId: '10294932049023',
            domaine: 'dolmen-tech.com'
          },
          {
            name: 'Catherine Barma',
            firstname: 'Catherine',
            lastname: 'Barma',
            email: 'catherine.barma@dolmen-tech.com',
            photo: 'http://img.tvmag.lefigaro.fr/ImCon/Arti/74366/Catherine-Barma-350.jpg',
            plusId: '142352342345234',
            domaine: 'dolmen-tech.com'
          },
          {
            name: 'Jean-Pierre Foucault',
            firstname: 'Jean-Pierre',
            lastname: 'Foucault',
            email: 'jp.foucauld@dolmen-tech.com',
            photo: 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBwgHBgkIBwgKCgkLDRYPDQwMDRsUFRAWIB0iIiAdHx8kKDQsJCYxJx8fLT0tMTU3Ojo6Iys/RD84QzQ5OjcBCgoKDQwNGg8PGjclHyU3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3N//AABEIAI8AyAMBIgACEQEDEQH/xAAcAAACAgMBAQAAAAAAAAAAAAAFBgAEAQMHAgj/xAA7EAACAQMDAgMFBgUCBwEAAAABAgMABBEFEiEGMRNBURQiYXGBBzJCkaHBIzOx0eEVUjRDYnKC8PEl/8QAGgEAAgMBAQAAAAAAAAAAAAAAAwQAAQIFBv/EACURAAICAgIBBAMBAQAAAAAAAAABAhEDIQQxEiIyQVEFE7EUcf/aAAwDAQACEQMRAD8A5Cy7TtrxtohdQ7eQKpstM1QFM8xDDURiHFDwMHNErX3hmrRUjeE4oZdpgmjaJx2qhqMOFJFakjMXsFx8MKMQruhBoOBhhRuxG6DFZijUj1bL96rcOlz6hP4FsPeBwSfw/M+Va4IpArNFE0j/AIVHY/49addP1iw0ayiaW7sX8VchCMmTB5VSM5wTxmhZ8/61Uds3hxebtvRU07pdbO3Jt2RtQONzSXHhHb8FAJUHjv3pnstLMSM1+wdg23w7S4dQcebZ4zmgK9ZW2oF4p7ZyrHG9Y1TcB8Xx+Rr3a61ZQo0mnWazs53HdfIvPyUYrjzlOV+XZ04xS6GyOPZLHNH7WQV2mMztIpPkfL86IXjeNbbJY/ClAzv8RV5/PNc61C+6w1Q+FpkUllCTw0BCn6tQyTofqu4j3XGoNJySRK+e/n/isKC+ZFu/hBvqQ29y7Jeq1zldpZpc/LlexpAu7GO2uf4szLbgbi23LDPZfQt5elHYdC1TTZCk4K5GCq9vniuj6bNpq2VlZavpNrPaSW67yyBvexz5c01g5P6/S3aAZcPnv5OKpGkkmUjEajsM5P1PmasCPJrpvXX2fWOn6c2sdPbhAoBa2QFlC4+8DnIx5/tXOmTatdaDUlaOdK0ypKKrBS74A71blFWdKtC7h2HHyrVFXSN9rbCKP417dcVfMYA7VVmFaoxewfKMV0L7Nji1Y+YrntxxT39nkmLKQ9sClOV7Bnje8ZJde02+1ObQb4IHMeQr9nz+9SuSda3RPUU0quQ6YAYNyKlBXHVKmGeam9Gq4g3pkUImj2uRTJEviLzQ+/tirFsflXQcRGLA22rliQTitbp6ivVt7ko9M1lKmbYbhTIFaNQhzGTV60XcgrbdQboiDRKBXTEyRdrjHrRvSwDEc+VDLyPZIcUW0JipJUkOORjvWIrYSb9JavlXT9BuL0PulklSKExvgDPJPxyB2qn0z0hf9Qjx2dbayz/MZclv+0efzozPcXV/1vp2lalFGI7Fs7Rzv4yCx8+9dKRxLMzxDCHhV8gPhXH5nJlCTrs6XGwqS30A9E+z3RLEq9wJb2XHeU8D5AU42Wk2NvjwraFCO22MCtdsiq2c9vXvRO3PuE0jGUpu5OxxpRWjJgVQDxVScANk/WrbPuHCkiqsynOT2q8iSWio38lC6iR1wygmg17emxkitZoUeCY7InI95D6f+8/Oj84wvNU9SSNrDeShwMHfyKHDuiZOgV0d1YYL+40HVHUgkmANgjOfu/EEHg0jdSWUOn61fWdsMQwzMsa/7V7gfTtSxqGqTDqJbrIDQz7lI5BIb9KZNVd5ru4ml3eJK5Y7u+TXoOJajs5PIqwN4Zkl2imOztRBAo8/OtGjWJdzKR7o4FGjHtXFOCjYPkTiqE49aJXA4oXePt71GUgVdv72B6079FOINMl/7SaSHAGWbimXp6fbpcxHkppTlewc4/uEzqSfx9YuXz+LFSqd+d97Ox82NStLSKfY02Z5ANXLm2EkRx6UPsyWxR6zAlQKeTinErE2KNxCUcgitG3BzTFqtkVbdjzoM8WODWHHYRS0FtKIeNaLeDuQ5oJox2vsNM0a5WtoHLsSNag2StxjzrdoB/ieY48jRHqS2wS3nihmhf8AFbay1sJ3EZL6A23XU7OqxzRwQxeuSUBz+WB86OXvUUejyC2RN8wTe69/kP6n6Vb1fR4p+qH1gbjbXccMkDEfeKpgj6Y/WqF3ol0b2e/tVDTSYZO2VA77c/i9M8V5vleLz+o7fGv9WjZY/aBCky+0WcyK348HPf0xwKd9O1OO9i3xLkMMjNJy6Pq93dRpPbaaLQqHkdzI88Z9N2fePxAAHpRTpjSrvSLtoJp/EgmAdFP3oj5j4ihySjuIaLtOwpqmuxaShecNtxnjzNKd99pEEky29hYzySs2BwMGmLWLBtQlaKMxqEO4s67v0oE3TWow+G2lwaPhyRc+1Wpckk/hYcn9KrG1J1ImTS0TS+q5bqaS31OBrV8e4SMg89ias6/dvbaRLIwLwDmQDgqPUVVg6bumOyULDGG/lh9+0D/ax9fQ9vWixjjEfs9woeLaUZW5yvxoMmlkRb9pwjVM/wCoySBgULbs+RHqafby1kN8YnWTc2CN45IIBB4oB1fosNrrEcWnn3JlyF3ZCmug2+lmC4i3oVaK2iTDHJztHP8AivQ8Saekcbkxaj5P7PFpZrDAq/tWqdcE0UkXA+VDrjuadEQTdkIuaCS5kYk/dFE75i7YB4oRqEwiXYODUZuKBt/MBkA0e6dZv9HnP/TSjO5diSfOm/p8f/hT4/20nyHcUN4dMTbr/iJPnUr3JGz3EgAySeMVKKZsZpLZ7W4ww4PnijFg2NvxojqmnCZNyj3hQm0DRPsfuDTaErsL3VoLiDGAaWLq0MbkYp009t8e00P1eyAJcLx6VRExWtl8GdW+NNtmAyAj0panQq/bzpi0V98YzV9FyK+v2viWzH50p6Wvh6gqkcZrot5biS1YEUh+H4OrAEcZxVdlxY+X2pXNp03bbVMkMV8iN6osnA59MgcfGmbT2XbtCDZ6UvCwOp9O39qrBWMAkGfVHVgf0q1pd6VSMseWAOPpXn/ykVHMmjtfjm5YqY1LGSmVAAzVLAOqAI4OxT2rHthfZCj4ZvP0HmaW5usLfSdbSyv7Wa3b3lEki+65B4IPmDSSXl0h3rsZ1UpfO7FQpHFEIkEiboiCp8qS9Q6ta41OODR7CW+uDHuYREKqDPdie1FGuZ9Lhju5HIhZQJ1B3CJvXPmv9KtenbWiu9WFr1RGhC4Ge9KmtXTwQzyxgs0aFj8h6fSimoaopjDK2Q3YZoWw8d/fGd42kY754oMpJzRvxpCz0Dpqa5rVzrt+g9ngk/gRE5AwMk/HsB86dHJkkeZhhpGLEfP96KHTrSx05oLSBI4FRIkVBgep58/80PlGBXoeFH0uf3/Dh82dtQXS/pRnoLqLcEDvRe7kCKTQK5IG6SSn0IAi9kEEZJJBNK93KZZGPkTRPVbszOQvbNCWU1Ug0Y1s0Feadenbd5NGZEBLMMADzpUhgZ27V0vpGGSGzHgwmRwo4ApTkL0oYwu5UVemuj4bZze6oF3AnYhHA+dSqnUusX9zNJalGghRveXszf4rNWsM5bbKeaEdJDTGizRgjmgerac0b+NGpx6Vq6P1sToIZ298DzPem+W3SeM8dxTd0Iq0xX02XkfCjNzAJ4Dxzihk1q1ncHH3DRmyYMuPWrbKEu/tSkpU96saG2yYoaOa5Yd5AKBWwMNyGAq+0XY1+HvhIPpSFrtsbfUQx9a6NZ+/Ep+FK3WNrtKyj15qkWhj6c96GP1x+1V47Tc8eF7NjArd0k260ib0FWrgez3k0B9whhLEx81Y5H65H/2uP+Wx2lL6Or+MnUnH7PZCWkbytmPsDuIGKr3LaRqcLWeqNaTwPyUkfOPiMdjV2f2e8RRdQRSc+8kihlz64NVdun2zGO4tolA/D4YZCPhXJh4p6Z1kvLTK9ja9O9KWEtvY3EUKTvvbxJN7OfLnvgc4qxY6nYX8vgW13bzNg+6HB4+VRG0ZWY2lrGsh80hyTVyC1gZQ8tvGN3/SAR9cc0TJ4yd3bK8VFaBsmmLBCBGMr4nug88Zqre21zPcxadYyiC5uWP8U/8AJQcu/wAwO3xxR2eaMPHnGFqloWL65utUByhYwW5H+xT7x+rcf+NXxcH7Mq+gPJzPHibClztykcLMYYkCR59AO/zPeh9zwMmiMgwDQq+bggV6SKS0jzspN7YHuzvY5PuilXXL3GUQ8UY1y+WFSiYyfjShKWlcs1FJFFNlLNk17gtnlParcFm8jduKN2lgFQAAZqJGnMHW1ntC5HNdN6NVbe0kdhnOOPgKTfZwnJps0x/B0tiO+P2pblRuKQbjS9TZt1/SbLqa3e4s3RblCQHHw7hqlI9hrV1o2oSXEBDI7nxIieHH7H41KyseSOovRvzxT3JbFHTrpredXjOCDXWOmNXS/tgHI3qORXNtT0h7dzJGpIrZoGpSWVyrjjB5HrTdfAo97R1u+sVuIeB9aGWgMUmxu4orouoR6haqykE45GfOvWoWf/MQYIrN1pmWvk1zwrPb8jPFKd7amGU8U4WjcbTVPWLLeN4FXF0ymjTokniQKD3HFaOqbXxbMkDtWNKKwTMsjBVHmTgCtmta9paWzRGUyNjnYOB9azLJCD2zcISktIx0Sc2qqSOM96Rut+v5LjqpV011fTrHdAuRjxifvt8sjj5fGquu9Tez2Mmn2DNGkn8x/Nh6fKufycNxSmaccuvgbwweN38nbei+qLPWzLAQ0c8YB2yea+op4SCyuIwsq58++K+YbO8mtZ0nt5WjmjOVde4NOlh9pOqQgC5RJF8ygwa5eTiOLuG0dTHyVJerR26G2sYE/gRqM98Emhmsala2NuZppwiZ2gZrldz9p18y4t7OJV9ZHJ/QUr6l1DqGp3Qnv5RLs+5HjCL8hWVxZy70W88I9Ox96l62heOS3s2JuZfd3DtEv96IfZN1THeQyaBcDbNb7ntWH4485Kn4jv8AGuOyTszE5yxOSa26XfXGm30F7ZyGOeFw6MPI09x8UcPtE8+R5VTPpi6batL2sXi28TEH3scVW0jrC21qzDylIbzHvR5wGPqv9qDapJLczncCOflXSxSjLpnMyQlF7QIu2a4l3EV7tdOaY9qJWWntK/3Ttpgt7BY1GFozdGLBFvpqxqBjmrBhEY4oq6Kg4FUbggA1VmQfJgnFGWnWDSHZjgY/aly+ukibuKzq16zaMe/al+R2hnj/ACKmo3pd2CnualDJGzyalEtkSOrz2KyqQwyDStq2gyQnxohxnJwKdbWXBCTD60M6o6h03RV8CUeLdMgYRAHCg9ix/atSyKKtgIxbegL0xq0un3IRgcZ94U+X3WGiWcO24ufElx/KjGT+dcXvOpJndygjj3HkRjH60Gmv5ZM4JGe/nSmTO5e1DcMKXZ1G9+0OCN39jtdo8jI2f0oDf/aHqFxkNMVX0XikMyO3djXk/GgO5dsKkl0hkuurb2VSqs3PnmhMuqXErEs5Oe9Ue9SqSSLtmyaQynJzWphmsmp5VZDWV4yKitjvXtfSvQMecEDNWQ8sR5GsAFhnsKOWGkWk9uZXL8Y53jk0JnaNZ3SPlFOFPwrKkm6QSUGoqT+TSQMcVFr0awKsGW7S+ktmBQn6GmTT+qHiYLI29T5PyKUamcefNVRLOr2fWECqp9njJ8wrFTTBYdSaZe+6r+DIT2kIx+dcMSd17GrEWoTRsCD862smSPTv/piWOEvijul0wRS5YFfXPH50t6pqIBKx8UkWnVFxEArSNtHkTkUTj1S3vlAEgR/PPY0zDkLqQCXHfwzXcTs8gLE/eo1fo0miqFBztqjFp4Z0Z+ckdjTNfQiPR1AA+7UzblE1iVRZz72QAe/zWaLeCZGCRKXc9gB3qUegds6aLdXXBFcc+0aXPV97Gh/lpHG2DnkIM11Kz1yOCJ5b0FBCpkYn0AzXDb65e9vbi7kJLzytISfic0vyHSo1x12zRUqVmlRoxUNSpUIZFYyfKs1PnUKMCvQBwTtOAcZA4ryfWrlu5/065QS7FO0umcBueKhZS86hTAzjJz39KyrYwD2rfs/hN2/PuasgasLoRdPXjAg42qq8fePnS8q7+T940Z1lFgsLRPAW3lf3nVMgEDzPPehgA7jtWIJdhctqo/RqKlTg1itknetli2y7jZUV8fgddwP0rQIr1K2Sj+I3AGSe1ec1CHmsipUqEJ2r0kjoco2DXisVCDDo3UclmwW4BlhALYzyD8K6r7HJq2k27WfvLPGGQ48iK4UDyK7t9kmpSXvR+BE0j6e5hbHmuNy4+nFaTapk8U7sLaNoFloUHtd2yNOFwZG/D8BWaCa1qc97KfG91V4Efp/msUz/AJvP1SYv/p8NQWgL9qkiWvT8UUXDXVxtOPNVGf61yjnJ/pTl9pd7NNf2VpMf5MJfA9WP9hSdQcz9bQTCqgYqVKlCCEqedZqVCEqVKmKhDH9KIaLLax38XtzlbRpFabCFiVVgcAD1wRz61QqflUIE72/tZNWvLm2g8O3mldoo0VQUUngc0NYZkiQZIGF/XyrGTkVahj3XduFGcKDz61G6LirYV6wR19i3cr4OFOQTgY747UFi95BTV1Ugk0+1bH4SefpSpFxQ8TuAXkKpsyJAjPuQPuXAz5fGrFr7GlhLLIzi9jlVo1HZ07N9c1Tbk1gjiiAScHkZyannWBWahCVDUqGoQwealSpUIY7Guv8A2DazFa2etWU4CqjR3IYd8YKn8uPzrj9Nn2cXLWusXhXO1rKQN+laivJ0Ry8dncOoOno9Uj9ssMeMwB44Dj+9SlHRes20mXwp1eWyJyy55T4j+1SjtZoemOwS/Rk9UtH/2Q==',
            plusId: '1231451234153234',
            domaine: 'dolmen-tech.com'
          }];

          var myContacts = new Contacts(contacts);

          app.main.show(new MainView({collection: myContacts}));

        }else{

          app.user.set({
            name: infos.name,
            firstname: infos.given_name,
            lastname: infos.family_name,
            email: infos.email,
            photo: infos.picture,
            plusId: infos.id,
            domaine: infos.hd
          });

          app.organisation.set({
            domaine: infos.hd,
          });

          console.log('post')

          $.ajax({
            url: 'http://localhost:3000/users',
            type: 'POST',
            data: app.user.toJSON(),
          }).done(function(data){
            console.log("post response");
            console.log(data);
          });

          app.main.show(new SignUpProfileView({model: app.user, app:app}));

        }

      });

    }

    function getContacts () {

      $.getJSON(
        'https://www.googleapis.com/plus/v1/people/'+infos.id+'/people/visible?access_token=' + gapi.auth.getToken().access_token + '&alt=json&callback=?',
        function(result){
          
          console.log(result);

          result.items.forEach(function(plusContact){

            if(plusContact.objectType === "person"){
              var person = {
                name: plusContact.displayName,
                photo: plusContact.image.url
              }
              plusContacts.push(person);
            }

          });

          console.log(plusContacts);

          $.getJSON(
            'https://www.google.com/m8/feeds/contacts/default/full/?access_token=' + gapi.auth.getToken().access_token + "&alt=json&callback=?",
            function(result){
              console.log(result);
              
              var contacts = result.feed.entry;
              var collegues = [];
              
              contacts.forEach(function(contact){
                
                var emails = contact.gd$email;

                emails.forEach(function(email){

                  if(isMyOrganisation.test(email.address)){

                    var collegue = {
                      name: contact.title.$t,
                      email: contact.gd$email[0].address,
                      photo: ''
                    };

                    plusContacts.forEach(function(plusContact){
                      if(plusContact.name === collegue.name){
                        console.log("concordance");
                        collegue.photo = plusContact.photo;
                      }
                    });
                    
                    collegues.push(collegue);

                  }

                });

              });

              collegues.forEach(function(collegue){
                $collegue = '<div><span>'+collegue.name+'</span><span>'+collegue.email+'</span><img src='+collegue.photo+' /></div>';

                $('body').append($collegue);
              });

            }
          );


        }

      );

    }

    this.checkAuth = function() {
      gapi.auth.authorize({ client_id: config.clientId, scope: config.scopes, immediate: false }, handleAuthResult);
    };

    handleClientLoad();
  };

  ApiManager.prototype.loadGapi = function() {
    var self = this;

    // Don't load gapi if it's already present
    if (typeof gapi !== 'undefined') {
      //return this.init();
    }

    require(['https://apis.google.com/js/client.js?onload=define'], function() {
      // Poll until gapi is ready
      function checkGAPI() {
        if (gapi && gapi.client) {
          self.init();
        } else {
          setTimeout(checkGAPI, 100);
        }
      }
      
      checkGAPI();
    });
  };

  Backbone.sync = function(method, model, options) {
    options || (options = {});

    switch (method) {
      case 'create':
      break;

      case 'update':
      break;

      case 'delete':
      break;

      case 'read':
      break;
    }
  };

  return ApiManager;
});