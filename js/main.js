require.config({
  paths: {
    jquery: 'libs/jquery/jquery.min',
    'velocity': 'libs/velocity/velocity.min',
    'velocity-ui': "libs/velocity/velocity.ui.min",
    underscore: 'libs/underscore/underscore.min',
    backbone: 'libs/backbone/backbone.min',
    marionette: 'libs/marionette/backbone.marionette.min',
    tpl: 'libs/tpl'
  },

  shim: {
    underscore: {
      exports: '_'
    },
    backbone: {
      exports: 'Backbone',
      deps: ['jquery', 'underscore']
    },
    marionette: {
      exports: 'Backbone.Marionette',
      deps: ['backbone']
    },
    velocity: {
      deps: ['jquery']
    },
    'velocity-ui': {
      deps: ['velocity']
    }
  },
  
  deps: ['jquery', 'underscore']

});

require([
  // Load our app module and pass it to our definition function
  'app',
  'gapi',
], function(app, ApiManager){

  app.start();
  app.apiManager = new ApiManager(app);

});
