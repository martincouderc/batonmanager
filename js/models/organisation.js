define([
  'underscore',
  'backbone',
], function(_, Backbone) {

  var Organisation = Backbone.Model.extend({

    // Default properties
    defaults: {
      name: 'mon organisation',
      email: 'monorganisation@dolmen-tech.com'
    },

    // Constructor
    initialize: function() {

    },

  });

  return Organisation;

});