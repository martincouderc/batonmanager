define([
  'underscore',
  'backbone',
], function(_, Backbone) {

  var User = Backbone.Model.extend({

    // Default properties
    defaults: {
      name: '',
      firstname: '',
      lastname: '',
      email: '',
      photo: '',
      contacts: [],
    },

    // Constructor
    initialize: function() {

    },

    // Any time a Model attribute is set, this method is called
    validate: function(attrs) {

    }

  });

  return User;

});