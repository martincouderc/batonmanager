// Filename: router.js
define([
  'jquery',
  'underscore',
  'marionette',
  'app',
  'views/signUpProfile',
  'views/signUpOrganisation'
], function($, _, Marionette, app, SignUpProfileView, SignUpOrganisationView){

  console.log("router");
  
  var Router = Marionette.AppRouter.extend({
    routes: {
      // Define some URL routes
      '/signupprofile': 'signUpProfile',
      '/signuporganisation': 'signUpOrganisation',
      '/users': 'showUsers',

      // Default
      '*actions': 'defaultAction'
    }
  });

  var router = new Router();
  
  router.on('signUpProfile', function(){
    var signUpProfileView = new SignUpProfileView();
    $('#page').html(signUpProfileView.render().$el);
  });

  router.on('signUpOrganisation', function(){
    var signUpOrganisation = new SignUpOrganisationView();
        $('#page').html(signUpOrganisation.render().$el);
  });

  router.on('defaultAction', function(actions){
    // We have no matching route, lets just log what the URL was
    console.log('No route:', actions);
  });

  return router;

});