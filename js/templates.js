/*global define */
define(function (require) {
  'use strict';

  return {
    homeView: require('tpl!templates/home'),
    contactListItem: require('tpl!templates/contactListItem'),
    signUpProfileView: require('tpl!templates/signup_profile'),
    signUpOrganisationView: require('tpl!templates/signup_organisation'),
    mainView: require('tpl!templates/main'),
  };

});