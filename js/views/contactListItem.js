define([
  'jquery',
  'underscore',
  'marionette',
  'templates',
], function($, _, Marionette, templates) {

    var ContactListItem = Backbone.Marionette.ItemView.extend({

        template: templates.contactListItem,

        initialize: function(options) {
          
        },

        signUp: function(){

        },

        onRender: function() {
        
        }
    });

    return ContactListItem;
});