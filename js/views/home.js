define([
  'jquery',
  'underscore',
  'marionette',
  'templates',
], function($, _, Marionette, templates) {

    var HomeView = Backbone.Marionette.ItemView.extend({

        template: templates.homeView,

        events: {
          'click .signin':'signUp'
        },

        initialize: function() {
                
        },

        signUp: function(){

        },

        onRender: function() {
        
        }
    });

    return HomeView;
});