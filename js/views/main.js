define([
  'jquery',
  'underscore',
  'marionette',
  'templates',
  'views/contactListItem'
], function($, _, Marionette, templates, ContactListItem) {

    var MainView = Backbone.Marionette.CollectionView.extend({

        className: 'contactListWrapper',

        childView: ContactListItem,

        initialize: function(options) {
          
        },

        signUp: function(){

        },

        onRender: function() {
        
        }

    });

    return MainView;
});