define([
  'jquery',
  'underscore',
  'marionette',
  'models/organisation',
  'templates',
  'views/main'
], function($, _, Marionette, Organisation, templates, MainView) {

    var SignUpOrganisationView = Backbone.Marionette.ItemView.extend({

        /*Template*/
        template: templates.signUpOrganisationView,

        events: {
          'click .signup_confirm':'confirm',
        },

        initialize: function(options) {
        
          this.app = options.app;
        
        },

        onRender: function() {
          //  ......
        },

        confirm: function(){
          this.app.main.show( new MainView({app: this.app}));
        }

    });

    return SignUpOrganisationView;
});