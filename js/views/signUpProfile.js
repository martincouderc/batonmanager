define([
  'jquery',
  'underscore',
  'marionette',
  'views/signUpOrganisation',
  'templates',
  'velocity',
  'velocity-ui',
], function($, _, Marionette, SignUpOrganisation, templates, velocity) {

    var SignUpProfileView = Marionette.ItemView.extend({

        /*Template*/
        template: templates.signUpProfileView,

        events: {
          'click .signup_confirm':'confirm',
        },

        initialize: function(options) {
          this.app = options.app;
        },

        onRender: function() {
          //  ......

          //this.$el.children()
          //.velocity("transition.slideUpIn", { stagger: 250 })

          var $imgProfile = $(this.$el.find('.signup_profile_img_wrapper')[0]);

          var $infosProfile = $(this.$el.find('.signup_profile_infos')[0]);

          //$imgProfile.css('transform', 'translateY(100px)');

          var intro = [
              { elements: $imgProfile, properties: { translateY:"0px", scale: .8 }, options: { duration: 1000, easing: [ 250, 15 ] } },
              { elements: $infosProfile, properties: 'transition.slideUpBigIn', options: { duration: 1000, sequenceQueue: false } }
          ];

          var img = new Image();
          img.src = this.model.get('photo');
          img.onload = function(){
            $.Velocity.RunSequence(intro);
          };

        },

        confirm: function(){
          this.app.main.show( new SignUpOrganisation({model: this.app.organisation, app: this.app}));
        }

    });

    return SignUpProfileView;
});