module.exports.getSchema = function() {

  var mongoose = require('mongoose');
  var Schema = mongoose.Schema;

  var organisationsScheme = new Schema({
    name:  String,
    email: String,
    logo: String,
    users: [{
      type: Schema.ObjectId,
      ref: 'users'
    }]
  });

  var Organisation = mongoose.model('organisations', organisationsScheme);

  return Organisation;

}