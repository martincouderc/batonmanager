module.exports.getSchema = function() {

  var mongoose = require('mongoose');
  var Schema = mongoose.Schema;

  var sticksSchema = new Schema({
    date:  Date,
    to: {
      type: Schema.ObjectId,
      ref: 'users'
    },
    from: [{
      type: Schema.ObjectId,
      ref: 'users'
    }],
    organisation: {
      type: Schema.ObjectId,
      ref: 'organisations'
    },
  });

  var Stick = mongoose.model('sticks', sticksSchema);

  return Stick;

}