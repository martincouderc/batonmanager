module.exports.getSchema = function() {
    
  var mongoose = require('mongoose');
  var Schema = mongoose.Schema;

  var usersSchema = new Schema({
    name:  String,
    firstname: String,
    lastname:   String,
    email: String,
    plusId: Number,
    domaine: String,
    photo: String,
    organisation: {
      type: Schema.ObjectId,
      ref: 'organisations'
    },
    contacts: {
      friends: [],
      wait: [],
      plus: [],
      gmail: []
    }
  });

  var User = mongoose.model('users', usersSchema);

  return User

}