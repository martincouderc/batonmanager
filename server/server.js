var express = require('express');
var bodyParser = require('body-parser');
var http = require('http');
var mongoose = require('mongoose');
var fs = require('fs');

var app = express();

app.use( bodyParser.json() );       // to support JSON-encoded bodies
app.use( bodyParser.urlencoded({
  extended: true
})); // to support URL-encoded bodies

var User = require('./models/users');
var users = User.getSchema();

var Organisation = require('./models/organisations');
var organisations = Organisation.getSchema();

var Stick = require('./models/sticks');
var sticks = Stick.getSchema();

mongoose.connect('mongodb://127.0.0.1:27017/test');

// fs.readdirSync(__dirname + '/models').forEach(function(filename){
//   if(~filename.indexOf('.js')) require(__dirname + '/models/' + filename);
// });

app.get('/users', function(req, res){

  mongoose.model('users').find(function(err, users){
    res.contentType('application/json');
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.send(users);
  });

});

app.get('/users/:user_id', function(req, res){

  mongoose.model('users').findById(req.params.user_id, function(err, users){
    res.contentType('application/json');
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.send(users);
  });

});

app.post('/users', function(req, res) {
  res.contentType('application/json');
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.send(req.body);

  var user = new users(req.body);

  user.save(function (err) {});
});

app.get('/users/gplus/:user_gplus_id', function(req, res){

  users.findOne({'plusId': req.params.user_gplus_id}, function(err, users){
  
    res.contentType('application/json');
    res.setHeader("Access-Control-Allow-Origin", "*");
  
    if(users){
      res.send(users);
    }else{
      var error = "Le user n'existe pas";
      res.send({error: error});
    }
  
  });

});

app.get('/users/gplus/:user_gplus_id/friends', function(req, res){

  users.findOne({'plusId': req.params.user_gplus_id}, 'contacts', function(err, contacts){
    
    console.log(contacts);

    //res.contentType('application/json');
    //res.setHeader("Access-Control-Allow-Origin", "*");
    //res.send(users);

  });

});

app.get('/organisations', function(req, res){

  mongoose.model('organisations').find(function(err, organisations){
    res.send(organisations);
  });

});

app.post('/organisations', function(req, res) {
  var test = req.body.test;
  res.send('merci pour le test');
  console.log('Post Request Accepted' );
});

app.get('/sticks', function(req, res){

  mongoose.model('sticks').find(function(err, sticks){
    res.send(sticks);
  });

});

app.post('/sticks', function(req, res) {
  var test = req.body.test;
  res.send('merci pour le test');
  console.log('Post Request Accepted' );
});

app.get('/', function(req, res){

  mongoose.model('users').find(function(err, users){
    res.send(users);
  });

});

app.listen(3000);